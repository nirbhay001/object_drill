function invert(obj){
    if(typeof obj!== 'object'){
        return [];
       }
    let newobj={};
    for(let key in obj){
        newobj[obj[key]]=key;
    }

    return newobj;
}

module.exports=invert;