
function defaultFunction(defaultProps, obj){
if(typeof defaultProps!=='object' || Array.isArray(defaultProps) ){
 return [];
}
for(let key  in obj){
    defaultProps[key]=obj[key];
}
return defaultProps;

}

module.exports=defaultFunction;