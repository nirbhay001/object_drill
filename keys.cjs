
function keys(obj) {
    if(typeof obj !== 'object'){
        return [];
    }
    let arr=[];
    for(let key in obj){
        arr.push(key);
    }
    return arr;
}

module.exports=keys;