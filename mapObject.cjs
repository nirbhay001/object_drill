
function mapObject(obj,cb){
    let newObj={};
    if( typeof obj !=='object' || typeof(cb)!=='function' ){
        return {};
    } else{
        for(let key in obj){
           let p=cb(obj[key],5);
            newObj[key]=p;
        }
    }
    return newObj;

    
}
module.exports=mapObject;