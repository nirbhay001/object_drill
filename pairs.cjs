function pairs(obj){
    if(typeof obj!= 'object'){
        return [];
    }
    let arr=[];
    for(let i in obj){
        arr.push([i, obj[i]]);
    }
    return arr;

}

module.exports=pairs;